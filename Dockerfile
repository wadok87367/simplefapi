FROM python:3.9-alpine3.15

COPY . .
RUN pip install --no-cache-dir --upgrade -r requirements.txt

EXPOSE 9050
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "9050"]
