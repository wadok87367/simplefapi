from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

# Complemente la variable definiendo su tipo usando typing
fakeDB = {}


class Item(BaseModel):
    name: str
    price: float
    is_offer: Optional[bool] = None


@app.get("/")
def read_root():
    # Documente que hace el código de esta función y proponga una mejor solución de código
    a = 0
    for i in fakeDB:
        a += 1 if fakeDB[i].price > 0 else 0
    return a


@app.get("/items/{item_id}")
def read_item(item_id: int):
    # Implemente un sistema de error en caso de que no exista el ítem solicitado
    return fakeDB[item_id]


@app.put("/items/{item_id}", response_model=Item)
def update_item(item_id: int, item: Item):
    # Implemente el código correspondiente para actualizar un ítem
    return item


@app.post("/items/", response_model=Item)
async def create_item(item_id: int, item: Item):
    # Implemente un sistema de error en caso de que el ítem ya exista
    fakeDB[item_id] = item
    return item
